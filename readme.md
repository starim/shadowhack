Shadowhack
==

This is a mini-game designed to provide a real, interactive computer interface
for players in the tabletop roleplaying game Shadowrun when they hack clusters
of files in the game.
