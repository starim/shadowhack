#![recursion_limit = "1024"]

extern crate cursive;
extern crate docopt;
extern crate env_logger;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate log;
extern crate rustc_serialize;


use std::path::PathBuf;
use cursive::Cursive;
use docopt::Docopt;

mod errors;
mod ui;

const USAGE: &'static str = "
Shadowhack.

Usage:
  shadowhack <initial-path>
  shadowhack (-h | --help)
  shadowhack --version

Options:
  -h --help     Show this screen.
  --version     Show version.
";

#[derive(Debug, RustcDecodable)]
struct Args {
    arg_initial_path: String,
}

fn main() {
    env_logger::init().unwrap();

    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.decode())
                            .unwrap_or_else(|e| e.exit());

    let mut ui = Cursive::new();

    match PathBuf::from(args.arg_initial_path.clone()).canonicalize() {
        Ok(canonical_initial_path) => ui::build_layout(&mut ui, canonical_initial_path),
        Err(error) => panic!(
            "Couldn't canonicalize initial path \"{}\": {}",
            args.arg_initial_path,
            error
        )
    }

    // TODO: doesn't appear to work
    //ui.add_global_callback('q', |a| a.quit());

    ui.run();
}
