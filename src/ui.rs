use std;
use std::borrow::Cow;
use std::error::Error as IOError;
use std::fs::{self, DirEntry, File};
use std::io::Read;
use std::path::{Path, PathBuf};
use std::vec::Vec;
use cursive::Cursive;
use cursive::traits::*;
use cursive::views::{Dialog, DummyView, LinearLayout, SelectView, TextView};

use errors::*;

pub fn update_status(app: &mut Cursive, entry: &PathBuf) -> Result<()> {
    let status_bar = match app.find_id::<TextView>("status") {
        Some(text_view) => text_view,
        None => bail!("No registered TextView named \"status\"")
    };
    let file_name = match entry.file_name() {
        Some(file_name) => file_name,
        None => bail!(
            "Couldn't get file name for menu entry \"{}\"",
            entry.to_string_lossy(),
        )
    };
    let file_name = file_name.to_string_lossy();
    let content = if entry.is_dir() {
        format!("{}: directory", file_name)
    } else {
        let metadata = try!(
            entry.metadata().chain_err(|| format!(
                "Couldn't get metadata for file \"{}\"",
                entry.to_string_lossy(),
            ))
        );
        let file_size = metadata.len();
        format!("{}: {} bytes", file_name, file_size)
    };
    status_bar.set_content(content);
    Ok(())
}

// Cursive doesn't allow callbacks to return errors, so the update_status callback is wrapped in a
// function that handles any errors returned then discards them. Errors in the status update
// callback are treated as non-fatal.
pub fn update_status_wrapper(app: &mut Cursive, entry: &PathBuf) {
    if let Err(ref error) = update_status(app, entry) {
        if let Some(ref mut status_bar) = app.find_id::<TextView>("status") {
            handle_recoverable_error(
                error,
                Cow::Borrowed("Error updating status display"),
                status_bar,
            );
        }
    }
}

pub fn load_file_contents(app: &mut Cursive, entry: &PathBuf) -> Result<()> {
    let text_view = match app.find_id::<TextView>("contents") {
        Some(text_view) => text_view,
        None => bail!("No registered TextView named \"contents\"")
    };
    let mut buffer = String::new();
    let _ = File::open(entry)
        .and_then(|mut f| f.read_to_string(&mut buffer))
        .map_err(|e| buffer = format!("Error: {}", e));
    text_view.set_content(buffer);
    Ok(())
}

pub fn transition_to_folder(app: &mut Cursive, entry: &PathBuf) {
    app.pop_layer();
    build_layout(app, entry);
}

pub fn add_parent_dir_to_picker<D>(
    view: &mut SelectView<PathBuf>,
    directory: &D,
    dir_name: &str
) -> Result<()>
    where D: AsRef<Path>
{
    // this will return None for the root path, so don't show an <up one level> menu entry in that
    // case
    if let Some(parent_path) = directory.as_ref().parent() {
        let canonical_path = try!(
            parent_path.canonicalize().chain_err(|| format!(
                "Couldn't canonicalize path \"{}\" which is the parent to \"{}\"",
                parent_path.to_string_lossy(),
                dir_name,
            ))
        );
        view.add_item("<up one level>", canonical_path);
    }
    Ok(())
}

pub fn add_dir_contents_to_picker<D>(
    view: &mut SelectView<PathBuf>,
    directory: D,
    dir_name: &str
) -> Result<()>
    where D: AsRef<Path>
{
    let dir_contents_iterator = try!(fs::read_dir(directory).chain_err(|| format!(
        "Can't read directory \"{}\"",
        dir_name,
    )));

    let (entries, errors):
        (Vec<std::io::Result<DirEntry>>, Vec<std::io::Result<DirEntry>>) =
        dir_contents_iterator.partition(|entry_result| entry_result.is_ok());

    if !errors.is_empty() {
        error!("============================================================");
        error!(
            "Couldn't read {} entries in directory \"{}\":",
            errors.len(),
            dir_name,
        );
        for error_result in errors {
            let error = error_result.unwrap_err();

            error!("------------------------------------------------------------");
            error!("{}", error);
            if let Some(cause) = error.cause() {
                error!("caused by: {}", cause);
            }
        }
        error!("============================================================\n");
    }

    let entries: Vec<DirEntry> = entries.into_iter().map(|entry| entry.unwrap() ).collect();
    let (dir_list, file_list): (Vec<DirEntry>, Vec<DirEntry>) = entries.into_iter().partition(|entry| {
        entry
            .file_type()
            .and_then(|file_type| Ok(file_type.is_dir()))
            .unwrap_or(false)
    });
    for entry in dir_list {
        let entry_text = format!("{}/", entry.file_name().to_string_lossy());
        view.add_item(entry_text, entry.path());
    }
    for entry in file_list {
        let entry_text = entry.file_name().to_string_lossy().to_string();
        view.add_item(entry_text, entry.path());
    }
    Ok(())
}

pub fn file_picker<D>(directory: D) -> Result<SelectView<PathBuf>>
    where D: AsRef<Path>
{
    let mut view: SelectView<PathBuf> = SelectView::new();
    let dir_name = directory.as_ref().to_string_lossy().into_owned();

    try!(add_parent_dir_to_picker(&mut view, &directory, &*dir_name));
    try!(add_dir_contents_to_picker(&mut view, directory, &*dir_name));

    Ok(view
        .on_select(update_status_wrapper)
        .on_submit(|app: &mut Cursive, entry: &PathBuf| {
            if entry.is_dir() {
                info!(
                    "Traversing into directory \"{}\"",
                    entry.to_string_lossy()
                );
                transition_to_folder(app, entry)
            } else {
                info!(
                    "Displaying file \"{}\"",
                    entry.to_string_lossy()
                );
                if let Err(ref error) = load_file_contents(app, entry) {
                    if let Some(ref mut status_bar) = app.find_id::<TextView>("status") {
                        handle_recoverable_error(
                            error,
                            Cow::Owned(format!(
                                "Error loading contents of file \"{}\"",
                                entry.to_string_lossy(),
                            )),
                            status_bar,
                        );
                    }
                }
            }
        }))
}

pub fn build_layout<D>(ui: &mut Cursive, directory: D)
    where D: AsRef<Path>
{
    let mut panes = LinearLayout::horizontal();
    let picker = match file_picker(directory) {
        Ok(file_picker) => file_picker,
        Err(ref error) => handle_fatal_error(
            error,
            Cow::Borrowed("Couldn't initialize file picker")
        )
    };
    panes.add_child(picker.fixed_size((30, 25)));
    panes.add_child(DummyView);
    panes.add_child(TextView::new("file contents")
        .with_id("contents")
        .fixed_size((50, 25)));

    let mut layout = LinearLayout::vertical();
    layout.add_child(panes);
    layout.add_child(TextView::new("status")
        .scrollable(false)
        .with_id("status")
        .fixed_size((80, 1)));

    ui.add_layer(Dialog::around(layout).button("Quit", |a| a.quit()));
}

