use std::borrow::Cow;
use cursive::views::TextView;

error_chain!{}

// logs the given error along with any chained errors and the backtrace if available, then updates
// the status display to show a brief error message
pub fn handle_recoverable_error(error: &Error, message: Cow<str>, status_bar: &mut TextView) {
    error!("============================================================");
    error!("{}: {}", message, error);
    for error in error.iter().skip(1) {
        error!("caused by: {}", error);
    }
    if let Some(backtrace) = error.backtrace() {
        error!("============================================================");
        error!("backtrace: {:?}", backtrace);
        error!("============================================================\n");
    }

    status_bar.set_content(format!("Error: {}: {}", message, error));
}

// logs the given error along with any chained errors and the backtrace if available, then causes
// the program to exit with error code 1
pub fn handle_fatal_error(error: &Error, message: Cow<str>) -> ! {
    error!("============================================================");
    error!("{}: {}", message, error);
    for error in error.iter().skip(1) {
        error!("caused by: {}", error);
    }
    if let Some(backtrace) = error.backtrace() {
        error!("============================================================");
        error!("backtrace: {:?}", backtrace);
        error!("============================================================\n");
    }
    ::std::process::exit(1);
}
