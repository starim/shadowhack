#!/usr/bin/env bash

set -eu

cargo build
RUST_BACKTRACE=1 RUST_LOG=shadowhack=info cargo run 2>log -- "$1"
